package testgrp

import (
	"context"
	"github.com/Budi721/ultimateservicego/foundation/web"
	"go.uber.org/zap"
	"net/http"
)

// Handlers manages the set of test endpoints.
type Handlers struct {
	Log *zap.SugaredLogger
}

func (h Handlers) Test(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	status := struct {
		Status string
	}{
		Status: "OK",
	}

	return web.Respond(ctx, w, status, http.StatusOK)
}
